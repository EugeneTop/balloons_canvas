import React from "react";
import "../style/CanvasWithCircle.style.css";
import {DrawService} from "../services/draw.service";
import ICircle, {Circle} from "../class/Circle.class";
import {Colors} from "../config/colors";
import {MoveCircleService} from "../services/moveCircle.service";

const MIN_RADIUS = 20;
const MAX_RADIUS = 50;
const COLORS = Colors;
const MAX_SPEED = 15;
const MIN_SPEED = 10;

interface Props {
    isAdd: boolean;
    isDeathMatch: boolean;
}

export class CanvasWithCircle extends React.Component<Props> {

    private canvas: HTMLCanvasElement;
    private drawService: DrawService;
    private circles: ICircle[] = [];
    private moveCircleService: MoveCircleService;

    private initializeCanvas = (el: HTMLDivElement | null) => {
        this.createCanvas(el);
        this.drawService = new DrawService(this.canvas);
        this.moveCircleService = new MoveCircleService(this.canvas);
        this.drawService.createContext();
        this.drawCircles();
        this.createTimeOutForUpdatePositionCircles();
    };

    private createTimeOutForUpdatePositionCircles() {
        setInterval(() => {
            if(this.circles === []) {
                return;
            }

            this.circles = this.moveCircleService.moveCircle(this.circles, this.props.isDeathMatch);
            this.drawCircles();
        }, 100);
    }

    private createCanvas = (el: HTMLDivElement | null) => {
        if(el === null) {
            return;
        }

        this.canvas = document.getElementById("canvasWithCircle") as HTMLCanvasElement;
        this.canvas.width = el.clientWidth;
        this.canvas.height = el.clientHeight;
    };

    private addCircle = () => {
        if(!this.props.isAdd) {
            return;
        }

        const circle = this.createCircle();
        this.circles.push(circle);
        console.log(circle);
        this.drawCircles();
    };

    private createCircle(): Circle {
        const radius = this.randomInteger(MIN_RADIUS, MAX_RADIUS);
        const maxX = this.canvas.width - radius;
        const maxY = this.canvas.height - radius;
        const x = this.randomInteger(radius, maxX);
        const y = this.randomInteger(radius, maxY);
        const quantityColors = COLORS.length - 1;
        const color = COLORS[this.randomInteger(0, quantityColors)];
        const speed = this.randomInteger(MIN_SPEED, MAX_SPEED);
        const angle = this.randomInteger(0, 360);
        return new Circle(color, radius, x, y, speed, angle);
    }

    private randomInteger(min: number, max: number) {
        return Math.floor(min + Math.random() * (max + 1 - min));
    }

    private drawCircles() {
        this.drawService.clear();
        this.circles.forEach((circle) => {
            this.drawService.drawCircle(circle);
        });
    }

    render() {
        return (<div className="canvasWithCircle" ref={el => this.initializeCanvas(el)}>
            <canvas id="canvasWithCircle" onClick={e => this.addCircle()} />
        </div>);
    }
}
