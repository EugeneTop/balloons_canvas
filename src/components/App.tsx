import React from 'react';
import {CanvasWithCircle} from "./CanvasWithCircle";
import {RightMenu} from "./RightMenu";
import '../style/App.style.css';

interface State {
    isAdd: boolean;
    isDeathMatch: boolean
}

interface Props {

}

class App extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            isAdd: false,
            isDeathMatch: false
        }
    }

    private updateIsAdd = () => {
      this.setState({isAdd: !this.state.isAdd});
    };

    private updateIsDeathMatch = () => {
        this.setState({ isDeathMatch: !this.state.isDeathMatch });
    };

    public render() {
        const { isAdd, isDeathMatch} = this.state;

        return (
            <div className="App">
                <CanvasWithCircle isAdd={isAdd} isDeathMatch={isDeathMatch} />
                <RightMenu isAdd={isAdd} isDeathMatch={isDeathMatch} updateIsAdd={this.updateIsAdd} updateIsDeathMatch={this.updateIsDeathMatch} />
            </div>
        );
    }
}

export default App;
