import React from "react";
import "../style/RightMenu.style.css";
import {FormControlLabel} from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";

interface Props {
    isAdd: boolean;
    isDeathMatch: boolean;
    updateIsAdd: () => void;
    updateIsDeathMatch: () => void;
}

export class RightMenu extends React.Component<Props> {
    render() {
        const { isAdd, updateIsAdd, isDeathMatch, updateIsDeathMatch } = this.props;

        return (<div className="menu">
            <div className="menu_header">
                <h1>Menu</h1>
                <FormControlLabel
                    control={
                        <Checkbox checked={isAdd} onClick={updateIsAdd} value="Add" />
                    }
                    label="Add"
                />
                <FormControlLabel
                    control={
                        <Checkbox checked={isDeathMatch} onClick={updateIsDeathMatch} value="Death match" />
                    }
                    label="Death match"
                />
            </div>
        </div>);
    }
}
