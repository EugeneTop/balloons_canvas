import ICircle from "../class/Circle.class";

export class DrawService {

    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;

    public constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas;
    }

    public createContext(): void {
        this.context = this.canvas.getContext("2d") as CanvasRenderingContext2D;
    }

    public drawCircle(circle: ICircle) {
        this.context.beginPath();
        this.context.arc(circle.position.x, circle.position.y, circle.radius, 0, 2 * Math.PI);
        this.context.fillStyle = circle.color;
        this.context.fill();
    }

    private saveAndRestoreContext(): void {
        this.context.save();
        this.context.restore();
    }

    public clear(): void {
        this.context.clearRect(0, 0,
            this.canvas.width, this.canvas.height);
        this.saveAndRestoreContext();
    }
}
