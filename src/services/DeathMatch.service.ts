import {Circle} from "../class/Circle.class";

export class DeathMatchService {
    public unionRectangle(circles: Circle[]): Circle[] {
        let newCircles: Circle[] = this.createCopyCircles(circles);
        circles.forEach((activeCircle, activeIndex) => {
            circles.forEach((circle, index) => {
                if(Math.sqrt((Math.pow(activeCircle.position.x - circle.position.x, 2))
                    + Math.pow(activeCircle.position.y - circle.position.y, 2)) + circle.radius <= activeCircle.radius && activeIndex !== index) {
                    if(activeCircle.radius > 100) {
                        newCircles.splice(activeIndex, 1);
                    }
                    activeCircle.radius += circle.radius;
                    newCircles.splice(index, 1);
                }
            });
        });
        return newCircles;
    }

    private createCopyCircles(circles: Circle[]): Circle[] {
        let clone: Circle[] = [];
        circles.forEach((circle) => {
            clone.push(circle);
        });
        return clone;
    }
}
