import ICircle from "../class/Circle.class";
import {DeathMatchService} from "./DeathMatch.service";

export class MoveCircleService {

    private deathMatchService: DeathMatchService = new DeathMatchService();
    private canvas: HTMLCanvasElement;

    public constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas;
    }

    public moveCircle(circles: ICircle[], isDeathMatch: boolean): ICircle[] {
        if(isDeathMatch) {
            circles = this.deathMatchService.unionRectangle(circles);
            this.checkOnContactWithWall(circles);
        }

        this.updatePositionCircles(circles);

        return circles;
    }

    private updatePositionCircles(circles: ICircle[]) {
        circles.forEach((circle) => {
            let radian = circle.angle * Math.PI / 180;
            circle.position.x = circle.position.x + circle.speed * Math.cos(radian);
            circle.position.y = circle.position.y + circle.speed * Math.sin(radian);
            if(circle.position.x <= circle.radius || circle.position.x >= this.canvas.width - circle.radius) {
                circle.angle = this.BounceWall(circle.angle, 0);
            }

            if(circle.position.y <= circle.radius || circle.position.y >= this.canvas.height - circle.radius) {
                circle.angle = this.BounceWall(circle.angle, 90);
            }
        });
    }

    private checkOnContactWithWall(circles: ICircle[]) {
        circles.forEach((circle) => {
            if(circle.position.x > this.canvas.width - circle.radius) {
                circle.position.x = this.canvas.width - circle.radius;
            }

            if(circle.position.x < circle.radius) {
                circle.position.x = circle.radius;
            }

            if(circle.position.y > this.canvas.height - circle.radius) {
                circle.position.y = this.canvas.height - circle.radius;
            }

            if(circle.position.y < circle.radius) {
                circle.position.y = circle.radius;
            }
        });
    }

    private BounceWall(direction: number, wallInclination: number) {
        return 180 - direction + 2 * wallInclination;
    }
}
