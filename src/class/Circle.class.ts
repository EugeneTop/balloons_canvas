interface IPosition {
    x: number,
    y: number
}

export default interface ICircle {
    color: string,
    radius: number,
    position: IPosition;
    speed: number;
    angle: number;
}

export class Circle implements ICircle{
    public color: string;
    public radius: number;
    public position: IPosition;
    public speed: number;
    public angle: number;

    public constructor(color: string, radius: number, x: number, y: number, speed: number, movingPositionNumber: number) {
        this.color = color;
        this.radius = radius;
        this.position = {
            x: x,
            y: y
        };
        this.speed = speed;
        this.angle = movingPositionNumber;
    }
}
